import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Home from '@/components/Home'
import API from '@/services/ApiService';
import store from '@/store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '',
      redirect: '/home'
    },
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    }
  ]
});

// Proteção das rotas
router.beforeEach((to, from, next) => {
    // Apenas as páginas de web são permitidas sem token
    let web = ["Login"];
    if(web.includes(to.name)){
        next();
    }else{
        API.token = store.getters.authToken
        API.post('/verifyToken', null).then(response => {
            if(response.data.auth === true){
                next();
            }else{
                router.push({
                    name: 'Login',
                    params:{
                        error: 'Faça o login para continuar.'
                    }
                });
            }
        }).catch(response => {
            router.push({
                name: 'Login',
                params:{
                    error: 'Há algum erro nas requisições ao servidor...'
                }
            });
        });
    }
})

export default router;